# Scala Workshop 2018

L'objectif pricipal de ce workshop n'est pas d'apprendre la programmation fonctionnelle (même si vous allez en voir un petit bout), mais de découvrir Scala "en douceur".

Nous voulons vous démontrer que:

- Scala n'est pas un langage compliqué
- Scala peut rester très facilement lisible (et nous militons pour)
- Scala est agréable à écrire

Notre souhait, c'est que, dans 1h, 2h, 3h, ... vous sachiez "faire" du Scala, et surtout que vous ayez envie de continuer.

## Les auteurs

- Philippe Charrière: https://twitter.com/k33g_org TAM at GitLab, CSO at Clever Cloud, raising 🤖 at Bots.Garden
- Nicolas Leroux: https://twitter.com/nicolasleroux CEO at Lunatech, déeveloppeur Scala de la 1ère heure
