# 07-functions

En Scala, on peut définir des fonctions (plus simplement qu'en Java):

```scala
object Hello extends App {
  def addOne(a: Int): Int = { a + 1 }
  def addTwo(a: Int): Int =  a + 2

  println(addOne(4)) 
  println(addTwo(4)) 

}
```

Il y a aussi bien sûr les lambdas:

```scala
object Hello extends App {

  val addThree = (a: Int) =>  a + 3
  println(addThree(4)) 

  val addFour = (a: Int) =>  a + 4 
  println(addFour(4))

  // if you want to specify the type:
  val addAndFive: (Int) => Int = (a) => a + 5

}
```


## Exercice

- addition
- division
- multiplication
- ▶️ https://scastie.scala-lang.org/k33g/I30WbYcWRfOE0Wi4g9zdYQ
